import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  fecha = new Date();
  dias : string[] = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
  meses : string[] = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

  diaSemana : number = this.fecha.getDay();
  diaMes : number = this.fecha.getDate();
  mesNumero : number = this.fecha.getMonth();
  anyo = this.fecha.getFullYear();
  
  dia = this.dias[this.diaSemana];
  mes = this.meses[this.mesNumero];

  mostrarFecha : string;

  constructor() {
    this.mostrarFecha = this.dia + ' ' + this.diaMes + ' de ' + this.mes + ' de ' + this.anyo;
  }

  ngOnInit(): void {
  }

}
